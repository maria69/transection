<?php

namespace App\Services\Transection;

use App\Models\Transection;

class FinnotechTransection
{

    public function __construct()
    {
        $base_url = "https://sandboxapi.finnotech.ir";
    }

    public function send($data)
    {
        $date = [
            "amount" => $data['amount'],
            "description" => "شرح تراکنش",
            "destinationFirstname" => $data['destinationFirstname'],
            "destinationLastname" => $data['destinationLastname'],
            "destinationNumber" => $data['destinationNumber'],
            "paymentNumber" => $data['paymentNumber'],
            "deposit" => $data['deposit'],
            "sourceFirstName" => $data['sourceFirstName'],
            "sourceLastName" => $data['sourceLastName'],
            "reasonDescription" => $data['reasonDescription']
        ];
        return $data;
    }

    public function fetch($userId)
    {
        $transections = Transection::whereUserSourceId($userId)->get();
        return $transections;
    }

    public function storeInProfile($data, $transferResualt, $userId)
    {
        $storeData = [
            'value' => $transferResualt['value'],
            'user_source_id' => $userId,
            'user_destination_id' => $transferResualt['user_destination_id'],
            'status' => $transferResualt['status']
        ];
        Transection::create($transferResualt);
        return true;
    }
}
