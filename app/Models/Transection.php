<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transection extends Model
{
    use HasFactory;

    protected $fillable = [
        'value', 'user_source_id', 'user_destination_id', 'status'
    ];



}
