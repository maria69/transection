<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transection\TransectionRequest;
use App\Http\Resources\Transection\TransectionResource;
use App\Http\Resources\Transection\TransferTransectionResource;
use App\Services\Transection\FinnotechTransection;
use Illuminate\Http\Request;

class UserTransectionController extends Controller
{
    /**
     * @var \FinnotechTransection
     */
    protected $transection;
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    protected $user;

    public function __construct(FinnotechTransection $finnotechTransection)
    {
        $this->transection = $finnotechTransection;
        $this->user = auth()->user();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'user_destination_id' => 'required',
        ]);
        $user = $this->user;
        $data = [
            "amount" => 1,
            "description" => "شرح تراکنش",
            "destinationFirstname" => "خلیلی  حسینی  بیابانی",
            "destinationLastname" => "سمیه   غز اله  فریماه",
            "destinationNumber" => "IR120620000000302876732005",
            "paymentNumber" => "123456",
            "deposit" => "776700000",
            "sourceFirstName" => $user->first_name,
            "sourceLastName" => $user->last_name,
            "reasonDescription" => "1"
        ];
        $transferResualt = $this->transection->send($data);
        $this->transection->storeInProfile($transferResualt, $data, $user->id);
        return response(
            'status' => true,
            'message' => 'transfer is successfulty',
            'data'=> new TransferTransectionResource($transferResualt);
        ,200);
    }

    public function index()
    {
        $user = $this->user;
        $transection = $this->transection->fetch($user->id);
        return response(
            'status' => true,
            'message' => 'get all transections',
            'data'=> new TransectionResource::collection($transferResualt);
        ,200);
    }
}
