<?php

namespace App\Http\Resources\Transection;

use Illuminate\Http\Resources\Json\JsonResource;

class TransectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        'result' => $this->id,
            'message' => $this->message,
            'transection_number' => $this->transection_number,
            'time' => $this->time,
    }
}
