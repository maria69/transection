<?php

namespace App\Http\Requests\Transection;

use Illuminate\Foundation\Http\FormRequest;

class TransectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            {


            }
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'user_destination_id' => 'required',
                    'value' => 'required',
                ];
            }
            case 'PUT':
            {
                return [];
            }
            case 'PATCH':
            {
                return [];
            }
            default:
                return [
                    //
                ];
        }
    }
}



